import React,{Fragment} from "react";
import Router from "./router/router"
import {PDFViewer} from '@react-pdf/renderer'
import Invoice from './components/reports/Invoice'
import {invoiceData} from './data/invoice'


function App() {
  return (
    <Fragment>
    <Router/>
    {/* <PDFViewer width="1700" height="900" className="app" >
        <Invoice invoice={invoiceData}/>
    </PDFViewer>  */}
  </Fragment>
  );
}

export default App;
