import React,{Fragment} from "react";
import {PDFViewer} from '@react-pdf/renderer'
import Invoice from './components/reports/Invoice'
import {invoiceData} from './data/invoice'


function InvoiceApp() {
  return (
    <Fragment>
    <PDFViewer width="1700" height="900" className="app" >
        <Invoice invoice={invoiceData}/>
    </PDFViewer> 
  </Fragment>
  );
}

export default InvoiceApp
