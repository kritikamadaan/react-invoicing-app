import React from 'react';
import { 
    BrowserRouter as Router, 
    Route, 
    Link, 
    Switch 
} from 'react-router-dom';
import Login from '../components/Login/login';
import SignUp from "../components/signUp/sigup"
import InvoiceApp from '../InvoiceApp';

function RouterComp(props) {
  return (
    <Switch> 
        <Route exact path='/' component={SignUp}></Route> 
        <Route exact path='/login' component={Login}></Route> 
        <Route exact path='/signup' component={SignUp}></Route> 
        <Route exact path='/invoice_app' component={InvoiceApp}></Route> 
    </Switch> 
  );
}

export default RouterComp;
