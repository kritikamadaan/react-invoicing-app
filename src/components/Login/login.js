import React,{useState}  from 'react';
import "./login.css";
import { NavLink } from "react-router-dom";
import userEvent from '@testing-library/user-event';

function Login(props) {
	const [email, setEmail] = useState("");
	const [password,setPassword] = useState("");
	const [confirmPassword,setConfirmPassword]= useState("")

	const submitHandler = () => {
		const data ={
			email:email,
			password:password
		}

	}
  return (
    <div>
    <form action="#" method="post">
     <h2>Login</h2>
		<p>
			<label for="Email" class="floatLabel">Email</label>
			<input id="Email" name="Email" type="text"   value={email} onChange={(e)=>{setEmail(e.target.value)}}/>
		</p>
		<p>
			<label for="password" class="floatLabel">Password</label>
			<input id="password" name="password" type="password" value={password} onChange={(e)=>{setPassword(e.target.value)}}/>
			<span>Enter a password longer than 8 characters</span>
		</p>
		<p>
			<label for="confirm_password" class="floatLabel">Confirm Password</label>
			<input id="confirm_password" name="confirm_password" type="password" value={confirmPassword} onChange={(e)=>{setConfirmPassword(e.target.value)}}/>
			<span>Your passwords do not match</span>
		</p>
		<p>
		<NavLink className="navbar-item"activeClassName="is-active"to="/invoice_app" exact >
			<input type="submit" value="Login My Account" id="submit" onSubmit={submitHandler}/>
		</NavLink>
		</p>
	</form>
    </div>
  );
}

export default Login
