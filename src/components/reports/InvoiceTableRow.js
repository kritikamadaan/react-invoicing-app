import React, {Fragment,useState} from 'react';
import {Text, View, StyleSheet } from '@react-pdf/renderer';

const borderColor = '#90e5fc'
const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        borderBottomColor: '#bff0fd',
        borderBottomWidth: 1,
        alignItems: 'center',
        height: 24,
        fontStyle: 'bold',
    },
    description: {
        width: '60%',
        textAlign: 'left',
        borderRightColor: borderColor,
        borderRightWidth: 1,
        paddingLeft: 8,
    },
    qty: {
        width: '10%',
        borderRightColor: borderColor,
        borderRightWidth: 1,
        textAlign: 'right',
        paddingRight: 8,
    },
    rate: {
        width: '15%',
        borderRightColor: borderColor,
        borderRightWidth: 1,
        textAlign: 'right',
        paddingRight: 8,
    },
    amount: {
        width: '15%',
        textAlign: 'right',
        paddingRight: 8,
    },
  });


const editInvoice = (id) => {
    console.log('edited row',id)
}
const InvoiceTableRow = ({items}) => {
    const [userItems,setUserItems]= useState(items);
    const [open,setOpen] = useState(false);

    const deleteRow = (id) => {
        console.log('delete row',id,userItems)
        let key= id
        const list = [...items]
    
        // Filter values and leave value which we need to delete 
       
        const updateList = items.filter(item => item.id !== id);
        const updatedItems = list.splice(id,1)
        // Update list in state 
        // this.setState({ 
        //   list:updateList, 
        // }); 
        // console.log('updaatedlist============',updateList)
    }
    const  editInvoice = (id) => {
        console.log('edited invoice',id)    
    }
     // Add item if user input in not empty 
//   addItem(){ 
//     if(this.state.userInput !== '' ){ 
//       const userInput = { 
//         // Add a random id which is used to delete 
//         id :  Math.random(), 
//         // Add a user value to list 
//         value : this.state.userInput 
//       }; 
  
//       // Update list 
//       const list = [...items]
//       list.push(userInput); 
  
//       // reset state 
//       setItems(list)
//     //   this.setState({ 
//     //     list, 
//     //     userInput:""
//     //   }); 
//     } 
//   } 
  
    const rows = items.map( item => 
        <View style={styles.row} key={item.sno.toString()}>
            <Text style={styles.description}>{item.desc}</Text>
            <Text style={styles.qty}>{item.qty}</Text>
            <Text style={styles.rate}>{item.rate}</Text>
            <Text style={styles.amount}>{(item.qty * item.rate).toFixed(2)}</Text> 
            {/* <button onClick={(e) => deleteRow(item.sno, e)}>Delete invoice</button> */}
            {/* <button onClick={editInvoice.bind(this, item.sno)}>Edit invoice</button>  */}
        </View>
    )
    return (<Fragment>{rows}</Fragment> )
};
  
export default InvoiceTableRow