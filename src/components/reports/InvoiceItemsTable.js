import React from 'react';
import {View, StyleSheet } from '@react-pdf/renderer';
import InvoiceTableHeader from './InvoiceTableHeader'
import InvoiceTableRow from './InvoiceTableRow'
import InvoiceTableBlankSpace from './InvoiceTableBlankSpace'
import InvoiceTableFooter from './InvoiceTableFooter'
import {invoiceData} from "../../data/invoice";

const tableRowsCount = 11;

const styles = StyleSheet.create({
    tableContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 24,
        borderWidth: 1,
        borderColor: '#bff0fd',
    },
});
console.log('dfhlkghkj',invoiceData.items)
  const InvoiceItemsTable = ({invoice}) => (
    <View style={styles.tableContainer}>
        <InvoiceTableHeader />
        <InvoiceTableRow items={invoiceData.items} />
        <InvoiceTableBlankSpace rowsCount={ tableRowsCount - invoiceData.items.length} />
        <InvoiceTableFooter items={invoiceData.items} />
    </View>
  );
  
  export default InvoiceItemsTable